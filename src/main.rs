use std::net::{UdpSocket, SocketAddr};
use std::error::Error;
use std::{str, env};
use std::process::exit;

fn main() {
    if env::args().len() != 4 {
        println!("Error: You have to give 3 args: <ServerId> <ServerAddress> <ServerName>");
        exit(1);
    }
    let id = env::args().nth(1).expect("ServerId to be given");
    let address = env::args().nth(2).expect("ServerAddress to be given");
    let name = env::args().nth(3).expect("ServerName to be given");
    let resp_str = format!("{{\"Id\":\"{}\",\"Address\":\"{}\",\"Name\":\"{}\"}}", id, address, name);
    let resp = resp_str.as_bytes();
    let mut socket = UdpSocket::bind("0.0.0.0:7359").expect("To bind to port");
    loop {
        let mut buf = [0; 22];
        let req = socket.recv_from(&mut buf);
        match req {
            Err(_) => { continue; }
            Ok((_, src)) => {
                if let Err(e) = respond(&mut socket, resp, buf, src) {
                    println!("Error: {}", e)
                }
            }
        }
    }
}

fn respond(sock: &mut UdpSocket, resp: &[u8], buf: [u8; 22], src: SocketAddr)-> Result<(), Box<dyn Error>> {
    let str = str::from_utf8(&buf)?;
    if str != "who is JellyfinServer?" {
        return Ok(());
    }
    sock.send_to(resp, src)?;
    println!("Got discovered by {}", src);
    Ok(())
}
